#!/bin/sh

chromium_path=~/.config/chromium
old_umask=$(umask)

if [ -e "$chromium_path" ]; then
  echo >&2 "$chromium_path exists. Skipping..."
  exit 1
fi

for cmd in jq sqlite3 unzip; do
  if ! command -v $cmd >/dev/null 2>&1; then
    echo >&2 "Command $cmd, not found."
    exit 1
  fi
done

umask 077 # sets files to 600 and dirs to 700
mkdir -p "$chromium_path"

touch "$chromium_path/First Run"

# Set 'chrome://flags/#extension-mime-request-handling'
cat >"$chromium_path/Local State" <<'EOF'
{
  "browser": {
    "enabled_labs_experiments": [
      "extension-mime-request-handling@2"
    ]
  }
}
EOF

mkdir -p "$chromium_path/Default"
if [ -e /etc/chromium/master_preferences ]; then
  >"$chromium_path/Default/Preferences.$$"
  cat /etc/chromium/master_preferences >>"$chromium_path/Default/Preferences.$$"
  jq -s -c -S '.[0] * .[1]' "$chromium_path/Default/Preferences.$$" preferences.json >"$chromium_path/Default/Preferences"
  rm -f "$chromium_path/Default/Preferences.$$"
else
  cat preferences.json >"$chromium_path/Default/Preferences"
fi

# Recreate 'Default/Web Data'
# sqlite3 -cmd '.dump' "$chromium_path/Default/Web Data" ''
sqlite3 -init web-data.sql "$chromium_path/Default/Web Data" '' 2>/dev/null

# Extensions
#mkdir -p "$chromium_path/Default/Extensions"
#for crx in extensions/*.crx; do
#  if [ ! -e "$crx" ]; then
#    break
#  fi
#  name="${crx%%_*}"
#  id="${crx##*_}"; id="${id%.crx}"
#  ver="${crx%_${id}.crx}"; ver="${ver#${name}_}"
#  mkdir -p "$chromium_path/Default/Extensions/$id/$ver"
#  unzip -qq "$crx" -d "$chromium_path/Default/Extensions/$id/$ver" 2>/dev/null
#done

umask $old_umask
